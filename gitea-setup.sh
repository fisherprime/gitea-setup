#!/bin/bash

# This script installs a gitea binary, sets up a gitea user & a gitea
# service.

# Queries Github's public API, gathering a repo's details.
_get_latest_release() {
	# TODO: Validate this operation.
	return "$(
		curl -s "https://api.github.com/repos/go-gitea/gitea/releases/latest" |
			grep '"tag_name":' | sed -E 's/.*"\S+":\s+"v+(\S+)".*$/\1/'
	)"

	# Github gist version.
	# grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' '])"'
}

_exec() {
	"$@" || return
}

_download_gitea() {
	local
	gitea_source="https://dl.gitea.io/gitea/$LATEST_GITEA/gitea-$LATEST_GITEA-linux-amd64"

	wget -O "$CACHE_DIR/gitea" "$gitea_source"

	# Set gitea executable bit
	chmod +x "$HOME/.cache/gitea"
}

_create_user() {
	adduser --system \
		--shell /bin/bash \
		--gecos 'Git Version Control' \
		--group \
		--disabled-password \
		--home /home/"$GITEA_USER" "$GITEA_USER"
}

_create_directories() {
	sudo mkdir -p /var/lib/gitea/{custom,data,log}
	sudo chown -R "$GITEA_USER":"$GITEA_USER" /var/lib/gitea/
	sudo chmod -R 750 /var/lib/gitea/
	sudo mkdir -p /etc/gitea
	sudo chown root:"$GITEA_USER" /etc/gitea
	sudo chmod 770 /etc/gitea
}

_export_gitea_workdir() {
	[[ -z $GITEA_WORK_DIR ]] &&
		export GITEA_WORK_DIR=/var/lib/gitea/
}

_globalize_gitea_executable() {
	sudo cp "$CACHE_DIR/gitea" /usr/local/bin
}

# Note: not the best function name.
_copy_system_files() {
	_globalize_gitea_executable

	cp gitea.service /etc/systemd/system
	sudo systemctl daemon-reload
	# sudo systemctl enable gitea.service

	cp gitea.sh /etc/profile.d
}

_enable_gitea_service() {
	sudo systemctl enable gitea.service
	sudo systemctl restart gitea.service
}

_setup_postgres() {
	local postgres_config_file="/var/lib/postgres/data/pg_hba.conf"
	local postgres_config_contents

	postgres_config_contents="$(cat $postgres_config_file)"

	sudo -u postgres createuser -P "$GITEA_USER"
	sudo -u postgres createdb -O "$GITEA_USER" "$GITEA_USER"

	# create user gitea;
	# create database gitea owner gitea;

	if [[ $postgres_config_contents != *"gitea"* ]]; then
		printf "local    gitea           gitea           peer\n" >> \
			"$postgres_config_file"
	fi
}

CACHE_DIR="$HOME/.cache"
GITEA_USER="gitea"

main() {
	[[ ! -d $CACHE_DIR ]] && mkdir "$CACHE_DIR"

	_download_gitea

	_exec _create_user
	_exec _create_directories

	_export_gitea_workdir
	_exec _copy_system_files
	_exec _enable_gitea_service

	sudo -u gitea GITEA_WORK_DIR=/var/lib/gitea /gitea web -c /etc/gitea/app.ini
}

LATEST_GITEA="$(_get_latest_release)"
main
