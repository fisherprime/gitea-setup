#!/bin/bash

_update_dir_permissions() {
	chmod 750 /etc/gitea
	chmod 640 /etc/gitea/app.ini
}

_update_dir_permissions
