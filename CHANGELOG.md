<a name=""></a>
##  (2020-02-25)


#### Features

* ***:**
  *  Add `_get_latest_release` operation ([94f8e650](94f8e650))
  *  Add inital gitea setup files ([fb3e24b0](fb3e24b0))
* **CHANGELOG.md:**  Add CHANGELOG ([ee4c1e2b](ee4c1e2b))
* **gitea-setup.sh:**  Add `LATEST_GITEA` variable ([20e65603](20e65603))
