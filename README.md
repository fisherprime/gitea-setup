# Gitea setup

**<gitea-scripts> folder hosting this README**
**<machine-ip> IP for machine to host gitea server**

```sh
scp <gitea-scripts> <machine-ip>:~
```

## In machine to host gitea server

```sh
cd gitea
gitea-setup.sh
gitea-post-setup.sh
```

# PostgreSQL setup

```psql
create role gitea;
create database gitea owner gitea;
```

# Post install recommendations

## Gitea config

This section helps if this was forgotten during setup.

**<server-domain> is the IP / domain name for the Gitea server**
**<server-port> is the port used to access the Gitea server'
s webUI**

Edit /var/lib/gitea/custom/conf/app.ini:

    - Under repository, set SSH_DOMAIN & DOMAIN to <server-domain>
    - Under ROOT_URL: set to http(s)://<server-domain>:<server-port>/

## SSH config

Permit the gitea user group in /etc/ssh/sshd_config:

    - Append `gitea` to `AllowGroups`

## Firewall config

**<ssh-port> SSH port**
**<network-segment> network segment to allow inbound connections from**

```sh
ufw allow in from <network-segment> to any port <ssh-port>
```

# Fixes

## Git hooks decline: pre-receive, update, post-update

From the admin dashboard, resynchronize the hooks for all repositories.
